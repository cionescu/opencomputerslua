m=component.proxy(component.list("modem")())
m.open(2412)
computer.beep(2000)
local eventStack = {}
local listeners = {}
event = {}
function event.listen(evtype,callback)
 if listeners[evtype] ~= nil then
  table.insert(listeners[evtype],callback)
  return #listeners
 else
  listeners[evtype] = {callback}
  return 1
 end
end
function event.ignore(evtype,id)
 table.remove(listeners[evtype],id)
end
function event.pull(filter)
 if not filter then return table.remove(eventStack,1)
 else
  for _,v in pairs(eventStack) do
   if v == filter then
    return v
   end
  end
  repeat
   t=table.pack(computer.pullSignal())
   evtype = table.remove(t,1)
   if listeners[evtype] ~= nil then
    for k,v in pairs(listeners[evtype]) do
     local evt,rasin = pcall(v,evtype,table.unpack(t))
     if not evt then
      print("stdout_write","ELF: "..tostring(evtype)..":"..tostring(k)..":"..rasin)
     end
    end
   end
  until evtype == filter
  return evtype, table.unpack(t)
 end
end
function print(...)
 local args=table.pack(...)
 pcall(function() m.broadcast(2412, table.unpack(args)) end)
end
local rS = {}
event.listen("modem_message",function(_,_,_,_,_,cmd)
 table.insert(rS,cmd)
 computer.beep()
 computer.pushSignal("read_return")
end)
function read(pref,repchar)
 local a,_ = table.remove(rS,1)
 if a == nil then
  event.pull("read_return")
  return table.remove(rS,1)
 else return a end
end
local function splitonspace(str)
 local t = {}
 for w in str:gmatch("%S+") do
  table.insert(t,w)
 end
 computer.beep()
 return t
end
function sleep(n)
  local t0 = computer.uptime()
  while computer.uptime() - t0 <= n do computer.pushSignal("wait") computer.pullSignal() end
end
print("skex BIOS v0.1.2")
print((math.floor(computer.totalMemory()/1024)).."k total, "..tostring(math.floor(computer.freeMemory()/1024)).."k free, "..tostring(math.floor((computer.totalMemory()-computer.freeMemory())/1024)).."k used")
function error(...)
 print(...)
end
local cmds = {"q - quit skex",
"l <start> <end> - writes the lines from <start> (default 1) to <end> (default #b) to stdout",
"lc - writes the current line to stdout",
"s [line] - seeks to a line",
"so - writes the number of the current line to stdout",
"i - inserts/appends lines, a line with only . to exit this mode",
"e - executes the code in the buffer",
"ei - interactive lua mode, works like the OpenOS lua program",
"d - deletes the current line",
"cb - clears the whole buffer",
"r - replaces the current line",
"fb - re-flashes the BIOS from input, . to end and flash the EEPROM",
"? / h - prints this help message"}
local run = true
b = {}
l = 1
while run do
 local cmdt = splitonspace(read())
 if cmdt[1] == "q" then
  run = false
 elseif cmdt[1] == "l" then
  startn,endn = cmdt[2],cmdt[3]
  if cmdt[2] == nil then
   startn=1
  end
  if cmdt[3] == nil then
   endn=#b
  end
  print("Line "..startn.." to "..endn..":")
  for i = tonumber(startn),tonumber(endn) do
   print(b[i])
  end
 elseif cmdt[1] == "lc" then
  print(b[l])
 elseif cmdt[1] == "so" then
  print("Current line: "..l)
 elseif cmdt[1] == "s" then
  l = tonumber(cmdt[2])
 elseif cmdt[1] == "i" then
  c=read()
  repeat
   table.insert(b,l,c)
   l=l+1
   c=read()
  until c=="."
 elseif cmdt[1] == "e" then
  cd=""
  for k,v in pairs(b) do
   cd=cd..v.."\n"
  end
  print(pcall(load(cd)))
 elseif cmdt[1] == "ei" then
  c=read()
  repeat
   print(pcall(load(c)))
   c=read()
  until c=="."
 elseif cmdt[1] == "d" then
  print(table.remove(b,l))
 elseif cmdt[1] == "r" then
  b[l] = read()
 elseif cmdt[1] == "cb" then
  b = {}
 elseif cmdt[1] == "fb" then
  c=read()
  nb = ""
  repeat
   nb = nb .. c .. "\n"
   c = read()
  until c=="."
  eeprom = component.proxy(component.list("eeprom")())
  eeprom.set(nb)
  computer.shutdown(true)
 elseif cmdt[1] == "?" or cmdt[1]:sub(1,1) == "h" then
  print("Commands:")
  print("Arguments surrounded by []s are mandatory, arguments surrounded by <> are optional.")
  for k,v in ipairs(cmds) do
   print(v)
  end
 end
end
