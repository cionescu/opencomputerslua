m=component.proxy(component.list("modem")())
m.open(2412)
computer.beep(2000)
function print(...)
 local args=table.pack(...)
 pcall(function() m.broadcast(2412, table.unpack(args)) end)
end
function read(pref,repchar)
 if pref ~= nil then print(pref) end
 repeat
  evt,_,_,_,_,cmd=computer.pullSignal()
 until evt == "modem_message"
 computer.beep(200)
 return cmd
end
local function splitonspace(str)
 local t = {}
 for w in str:gmatch("%S+") do
  table.insert(t,w)
 end
 computer.beep()
 return t
end
function sleep()
 local deadline = computer.uptime() + (timeout or 0)
 repeat
  coroutine.yield()
 until computer.uptime() >= deadline
end
print("skex BIOS v0.1")
print((math.floor(computer.totalMemory()/1024)).."k total, "..tostring(math.floor(computer.freeMemory()/1024)).."k free, "..tostring(math.floor((computer.totalMemory()-computer.freeMemory())/1024)).."k used")
function error(...)
 print(...)
end

local cmds = {"q - quit skex",
"l <start> <end> - writes the lines from <start> (default 1) to <end> (default #b) to stdout",
"lc - writes the current line to stdout",
"s [line] - seeks to a line",
"so - writes the number of the current line to stdout",
"i - inserts/appends lines, a line with only . to exit this mode",
"e - executes the code in the buffer",
"d - deletes the current line",
"r - replaces the current line",
"? / h - prints this help message"}

local run = true
local b = {}
local l = 1

while run do
 local cmdt = splitonspace(read())
 if cmdt[1] == "q" then
  run = false
 elseif cmdt[1] == "l" then
  startn,endn = cmdt[2],cmdt[3]
  if cmdt[2] == nil then
   startn=1
  end
  if cmdt[3] == nil then
   endn=#b
  end
  print("Line "..startn.." to "..endn..":")
  for i = tonumber(startn),tonumber(endn) do
   print(b[i])
  end
 elseif cmdt[1] == "lc" then
  print(b[l])
 elseif cmdt[1] == "so" then
  print("Current line: "..l)
 elseif cmdt[1] == "s" then
  l = tonumber(cmdt[2])
 elseif cmdt[1] == "i" then
  c=read("insert")
  repeat
   table.insert(b,l,c)
   l=l+1
   c=read()
  until c=="."
 elseif cmdt[1] == "e" then
  cd=""
  for k,v in pairs(b) do
   cd=cd..v.."\n"
  end
  print(pcall(load(cd)))
 elseif cmdt[1] == "d" then
  print(table.remove(b,l))
 elseif cmdt[1] == "r" then
  b[l] = read("replace")
 elseif cmdt[1] == "fb" then
  c=read("BIOS re-flash")
  nb = ""
  repeat
   nb = nb .. c .. "\n"
   c = read()
  until c=="."
  eeprom = component.proxy(component.list("eeprom")())
  eeprom.set(nb)
  computer.shutdown(true)
 elseif cmdt[1] == "?" or cmdt[1]:sub(1,1) == "h" then
  print("Commands:")
  print("Arguments surrounded by []s are mandatory, arguments surrounded by <> are optional.")
  for k,v in ipairs(cmds) do
   print(v)
  end
 end
end
